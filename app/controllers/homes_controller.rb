class HomesController < ApplicationController

  include IssuesHelper

  before_action :logged_in_user, only: [:index, :assignee_issues]
  before_action :manager_logged_in, only: [:manager_issues]
  before_action :count, only: [:index, :manager_issues, :assignee_issues]

  def index
    # TODO Job补充
    if manager_logged_in?
      if @milestone_count > 0
        redirect_to milestones_path
=begin
        @milestone_ids = current_user.milestones.ids
        @issues_count = Hash.new
        @milestone_ids.each do |id|
          @count = Issue.where(milestone_id: id).count
          @issues_count[id] = @count > 1 ? @count.to_s << ' ISSUES' : @count.to_s << ' ISSUE'
        end
        render 'milestones/index'
=end
      elsif @manager_issue_count > 0
        render 'manager_issues'
      elsif @assignee_issue_count > 0
        render 'assignee_issues'
      else
        render 'milestones/index'
      end
    end

    if member_logged_in?
      render 'assignee_issues'
    end
  end

  def manager_issues
    @assignee_ids = current_user.manager_issues.pluck(:assignee_id)
    @assignee_info = assignee_info(@assignee_ids)
    @to_do = current_user.manager_issues.create_state
    @doing = current_user.manager_issues.start_state
    @done = current_user.manager_issues.close_state
    render 'issues/board'
  end

  def assignee_issues
    @to_do = Issue.create_state.where(assignee_id: current_user.id)
    @doing = Issue.start_state.where(assignee_id: current_user.id)
    @done = Issue.close_state.where(assignee_id: current_user.id)
    render 'issues/board'
  end
end
