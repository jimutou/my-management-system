class IssuesController < ApplicationController

  include IssuesHelper

  before_action :set_issue, only: [:change, :show]
  before_action :manager_logged_in, only: [:new, :change]

  def new
    @milestone_issue = Issue.new()
    @milestone_id = params[:milestone_id]
    @member_list = User.where(admin: false, manager: false)
    @member_collect = @member_list.collect {
      |member| [member.name, member.id]
    }
    @user_names = @member_list.pluck(:name)
=begin
    @memberlist.each do |member|
      member.id
      member.name
    end
    @usernames = @memberlist.pluck(:name)
    @userids = @memberlist.pluck(:id)
=end
  end

  def index
    @assignee_ids = Issue.where(milestone_id: params[:milestone_id]).pluck(:assignee_id)
    @assignee_info = assignee_info(@assignee_ids)
    @to_do = Issue.create_state.where(milestone_id: params[:milestone_id])
    @doing = Issue.start_state.where(milestone_id: params[:milestone_id])
    @done = Issue.close_state.where(milestone_id: params[:milestone_id])
  end

  def create
    @issue = current_user.manager_issues.build(issue_params)
    if @issue.save
      flash[:success] = 'ISSUE新建成功!'
      redirect_to home_manager_issues_url
      # redirect_to home_url
    else
      flash[:warning] = '信息填写有误,请重试'
      render 'new'
      # render 'static_pages/home'
    end
  end

  def show
    # @issue_detail = Issue.where(milestone_id: params[:milestone_id], id: params[:id])
  end

  def change
    @source_action_name = params[:source_action]
    if @issue.update_attributes(i_state: params[:state])
      flash[:info] = 'ISSUE状态更新成功'
    else
      flash[:warning] = 'ISSUE状态更新失败'
    end

    if @source_action_name == 'manager_issues'
      redirect_to home_manager_issues_url
    else
      redirect_to home_assignee_issues_url
    end
  end

  private
  def set_issue
    @issue = Issue.find_by_id(params[:id])
  end

  def issue_params
    params.require(:issue).permit(:name, :content, :due_date, :assignee_id, :tag)
        .merge(milestone_id: params[:milestone_id])
  end
end
