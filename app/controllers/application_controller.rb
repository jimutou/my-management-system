class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

  private

  # 确保用户已登录
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = '请登录'
      redirect_to root_url
    end
  end

  # 确保用户有管理员权限
  def manager_logged_in
    unless manager_logged_in?
      redirect_to root_url, flash: {danger: '您的操作需要项目经理权限！'}
    end
  end
  
  # top_area中的MIT的统计个数 TODO 下一步可能取消显示
  def count
    if manager_logged_in?
      # @issue_count = Issue.where(user_id: current_user.id).count
      @manager_issue_count = current_user.manager_issues.count
      if current_user.milestones.any?
        @milestone_count = current_user.milestones.length
        @milestone_items = current_user.milestones.paginate(page: params[:page])
      else
        @milestone_count = 0
      end
    end

    if member_logged_in? || manager_logged_in?
      # 被指派的issue
      @assignee_issue_count = Issue.where(assignee_id: current_user.id).count
      # @assignee_issues = Issue.find_by_assignee_id(current_user.id).paginate(page: params[:page])
    end
  end
end
