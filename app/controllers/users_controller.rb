class UsersController < ApplicationController
  before_action :logged_in_user, only: :update
  before_action :correct_user, only: [:update, :destroy]
  before_action :manager_logged_in, only: [:new, :create, :edit, :show]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_url, flash: { success: '新账号注册成功,请登陆' }
    else
      flash[:warning] = '账号信息填写有误,请重试'
      render 'new'
    end
  end

  def edit
    @user=User.find_by_id(params[:id])
  end

  def update
    @user = User.find_by_id(params[:id])
    if @user.update_attributes(user_params)
      flash[:info] = '更新成功'
    else
      flash[:warning] = '更新失败'
    end
    redirect_to home_path, flash: flash
  end

  def destroy
    @user = User.find_by_id(params[:id])
    @user.destroy
    redirect_to users_path(new: false), flash: {success: '用户删除成功'}
  end

  def show
    @user = User.find_by_id(params[:id])
    @milestones = @user.milestones.paginate(page: params[:page])
  end


#----------------------------------- students function--------------------


  private

  def user_params
    params.require(:user).permit(:name, :email, :employee_id, :password, :password_confirmation)
  end

# Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    unless current_user?(@user)
      redirect_to root_url, flash: {warning: '您没有相应的权限，进行操作'}
    end
  end

end