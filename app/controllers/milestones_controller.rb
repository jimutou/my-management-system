class MilestonesController < ApplicationController
  # before_action :member_logged_in, only: [:select, :quit, :list]
  before_action :set_milestone, only: [:edit, :update, :destroy, :start, :close, :show]
  before_action :manager_logged_in, only: [:index, :new, :create, :edit, :destroy, :update, :start, :close]
  # before_action :logged_in_user, only: :index

  def new
    @milestone = Milestone.new
  end

  def create
    @milestone = current_user.milestones.build(milestone_params)
    if @milestone.save
      flash[:success] = '项目里程碑新建成功!'
      redirect_to home_url
    else
      flash[:warning] = '信息填写有误,请重试'
      render 'new'
      # render 'static_pages/home'
    end
  end

  def show
  end

  def edit
  end

  def update
    # @milestone = Milestone.find_by_id(params[:id])
    if @milestone.update_attributes(milestone_params)
      flash[:info] = '更新成功'
    else
      flash[:warning] = '更新失败'
    end
    # redirect_to milestones_path, flash: flash
    redirect_to milestones_path
  end

  def destroy
    # @milestone = Milestone.find_by_id(params[:id])
    current_user.milestones.delete(@milestone)
    @milestone.destroy
    flash[:success] = "成功删除项目里程碑: #{ @milestone.name }"
    redirect_to milestones_path
  end

  #-------QiaoCode--------
  public

  def start
    # @milestone = Milestone.find_by_id(params[:id])
    if @milestone.update_attributes(m_state: Status::START)
      flash[:info] = '里程碑开启工作模式'
    else
      flash[:warning] = '开通失败'
    end
    redirect_to milestones_path, flash: {:success => "已经成功开启此里程碑:#{ @milestone.name }"}
  end

  def close
    # @milestone = Milestone.find_by_id(params[:id])
    if @milestone.update_attributes(m_state: Status::CLOSE)
      flash[:info] = '里程碑关闭成功'
    else
      flash[:warning] = '里程碑关闭失败'
    end
    redirect_to milestones_path, flash: {:success => "已经成功关闭此里程碑:#{ @milestone.name }"}
  end

  # def select
  #   @course = Course.find_by_id(params[:id])
  #   current_user.courses<<@course
  #   flash={:suceess => "成功选择课程: #{@course.name}"}
  #   redirect_to courses_path, flash: flash
  # end
  #
  # def quit
  #   @course=Course.find_by_id(params[:id])
  #   current_user.courses.delete(@course)
  #   flash={:success => "成功退选课程: #{@course.name}"}
  #   redirect_to courses_path, flash: flash
  # end


  #-------------------------for both teachers and students----------------------

  def index
    @milestone_ids = current_user.milestones.ids
    @issues_count = Hash.new
    @milestone_ids.each do |id|
      @count = Issue.where(milestone_id: id).count
      @issues_count[id] = @count > 1 ? @count.to_s << ' ISSUES' : @count.to_s << ' ISSUE'
    end
    @milestone_items = current_user.milestones.paginate(page: params[:page])
    @milestone_to_do = current_user.milestones.where(m_state: Status::CREATE).paginate(page: params[:page])
    @milestone_doing = current_user.milestones.where(m_state: Status::START).paginate(page: params[:page])
    @milestone_done = current_user.milestones.where(m_state: Status::CLOSE).paginate(page: params[:page])
  end

  private

  def set_milestone
    @milestone = Milestone.find_by_id(params[:id])
  end

  # Confirms a logged-in user as a member.
  def member_logged_in
    unless member_logged_in?
      redirect_to root_path, flash: { danger: '请登陆' }
    end
  end

  def milestone_params
    params.require(:milestone).permit(:name, :context, :start_date, :due_date)
  end
end
