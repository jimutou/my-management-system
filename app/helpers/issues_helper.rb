module IssuesHelper
  def assignee_info(assignee_ids)
    @assignee_info = Hash.new
    @assignee_users = User.find(assignee_ids)
    @assignee_users.each { |user| @assignee_info[user.id] = user.name }
    return @assignee_info
  end
end
