module Status
  AccessDeniedError = Class.new(StandardError)

  # milestone issue job status
  CREATE = 0
  START = 1
  CLOSE = 2

  # issue process
  # 未开始、分析、开发、测试、完成
  ISSUE_CREATE     = 10
  ANALYSIS  = 20
  DEVELOPING    = 30
  TESTING   = 40
  DONE     = 50

  class << self
    delegate :values, to: :options

    def all_values
      options_with_owner.values
    end

    def options
      {
          'OPEN'     => CREATE,
          'START'  => START,
      }
    end

    def options_with_close
      options.merge(
          'CLOSE' => CLOSE
      )
    end

    def sym_options
      {
          OPEN:     CREATE,
          START:  START,
          CLOSE:    CLOSE,
      }
    end

    def issue_options
      {
          'ISSUE_CREATE' => ISSUE_CREATE,
          'ANALYSIS' => ANALYSIS,
          'DEVELOPING' => DEVELOPING,
          'TESTING' => TESTING,
          'DONE' => DONE,
      }
    end

    def sym_issue_options
      {
          ISSUE_CREATE: ISSUE_CREATE,
          ANALYSIS: ANALYSIS,
          DEVELOPING: DEVELOPING,
          TESTING: TESTING,
          DONE: DONE
      }
    end

    def issue_values
      issue_options.values
    end
  end

  def target_close
    Access.options_with_close.key(access_field)
  end

  def close?
    access_field == CLOSE
  end

end