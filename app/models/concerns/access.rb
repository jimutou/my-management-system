module Access
  AccessDeniedError = Class.new(StandardError)

  NO_ACCESS = 0
  GUEST     = 10
  REPORTER  = 20
  MASTER    = 30
  MANAGER   = 40
  OWNER     = 50

  # Job protection settings
  PROTECTION_NONE          = 0
  PROTECTION_PART          = 1
  PROTECTION_FULL          = 2
  PROTECTION_DEV_CAN_MERGE = 3

  class << self
    delegate :values, to: :options

    def all_values
      options_with_owner.values
    end

    def options
      {
          'Guest'     => GUEST,
          'Reporter'  => REPORTER,
          'Master'    => MASTER,
          'Manager'   => MANAGER,
      }
    end

    def options_with_owner
      options.merge(
          'Owner' => OWNER
      )
    end

    def sym_options
      {
          guest:     GUEST,
          reporter:  REPORTER,
          master:    MASTER,
          manager:   MANAGER,
      }
    end

    def sym_options_with_owner
      sym_options.merge(owner: OWNER)
    end

    def protection_options
      {
          'Not protected: Both managers and masters can create new jobs, edit a job, or delete the job.' => PROTECTION_NONE,
          'Partially Protected: members only can click checkbox to confirm their jobs.' => PROTECTION_PART,
          'Fully protected: members cannot do anything but review what the issues.' => PROTECTION_FULL,
      }
    end

    def protection_values
      protection_options.values
    end
  end

  def human_access
    Access.options_with_owner.key(access_field)
  end

  def owner?
    access_field == OWNER
  end
end