class User < ActiveRecord::Base
  has_many :milestones
  has_many :manager_issues, class_name: 'Issue', foreign_key: 'user_id'
  # has_many :assigned_issues, class_name: 'Issue', foreign_key: 'assignee_id'
  before_save { self.email = email.downcase }
  # before_save { self.name = name.downcase }
  attr_accessor :remember_token
  validates :name, presence: true, length: {maximum: 50}, uniqueness: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # VALID_EMAIL_REGEX = /\A[\w+\-.]+@[asiainfo.com]+\z/i
  validates :email, presence: true, length: {maximum: 255},
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: {case_sensitive: false}

  has_secure_password
  validates :password, presence: true, length: {minimum: 6}, allow_nil: true

  # scope
  # scope :manager, -> { where(manager: true) }
  # scope :admin, -> { where(admin: true) }

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def user_remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def user_forget
    update_attribute(:remember_digest, nil)
  end

  # Returns true if the given token matches the digest.
  def user_authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def member_list
    User.where(admin: false, manager: false)
  end

end
