class Milestone < ActiveRecord::Base
  belongs_to :user
  has_many :issues
  # default_scope -> { order(:due_date) }
  validates :user_id, presence: true
  validates :name, presence: true, length: { maximum: 100 }
  # validates :content, presence: true
  validates :start_date, presence: true
  validates :due_date, presence: true
  validates :m_state, presence: true, length: { maximum: 1 }
end
