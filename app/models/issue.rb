class Issue < ActiveRecord::Base
  belongs_to :milestone
  belongs_to :user
  # belongs_to :manager, class_name: 'User'
  # belongs_to :assignee, class_name: 'User'

  # validates :owner_id, presence: true
  validates :assignee_id, presence: true
  validates :name, presence: true, length: { maximum: 100 }
  validates :content, presence: true
  # validates :start_date, presence: true
  validates :due_date, presence: true
  validates :i_process, presence: true, length: { maximum: 2 }
  validates :i_state, presence: true, length: { maximum: 1 }

  # scope
  scope :create_state, -> { where(i_state: Status::CREATE) }
  scope :start_state, -> { where(i_state: Status::START) }
  scope :close_state, -> { where(i_state: Status::CLOSE) }
end
