class CreateMilestones < ActiveRecord::Migration
  def change
    create_table :milestones do |t|
      t.string :name
      t.text :content
      t.references :user, index: true, foreign_key: true
      t.date :start_date
      t.date :due_date
      t.integer :m_state, default: Status::CREATE

      t.timestamps null: false
    end
    add_index :milestones, :due_date
    add_index :milestones, :m_state
  end
end
