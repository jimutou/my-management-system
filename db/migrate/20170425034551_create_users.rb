class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.string :employee_id
      t.string :password_digest
      t.string :remember_digest

      t.boolean :admin, default: false
      t.boolean :manager, default: false

      t.timestamps null: false
    end
    add_index :users, :email, unique: true
    add_index :users, :name, unique: true
  end
end
