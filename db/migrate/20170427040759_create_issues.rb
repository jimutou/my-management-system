class CreateIssues < ActiveRecord::Migration
  def change
    # drop_table :issues
    create_table :issues do |t|
      t.references :milestone, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      # t.references :assignee, index: true, foreign_key: true
      t.string :name
      t.text :content
      t.integer :assignee_id
      t.integer :i_process, default: Status::ISSUE_CREATE
      t.integer :i_state, default: Status::CREATE
      t.string :tag
      t.date :due_date

      t.timestamps null: false
    end
    add_index :issues, :i_process
    add_index :issues, :i_state
    add_index :issues, :due_date
    add_index :issues, :assignee_id

    # add_foreign_key :issues, :users, name: :owner
    # add_foreign_key :issues, :users, name: :assignee
    # add_foreign_key :issues, :users, column: :owner_id
  end
end
