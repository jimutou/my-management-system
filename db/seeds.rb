# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
# 姓名	工号	邮箱
# 林蓉	11306	linrong@aisainfo.com
# 鲜伟	13730	xianwei@asiainfo.com
# 赵慧敏	5421	zhaohm@asiainfo.com
# 李阳	23389	liyang@asiainfo.com
# 甘雪霞	13188	ganxx@asiainfo.com
# 朱国栋	13482	zhugd@asiainfo.com
# 李晨升	21213	lics5@asiainfo.com
# 徐岗	21434	xugang5@asiainfo.com
# 张宗琦	19281	zhangzq10@asiainfo.com
# 戴鹏达	27402	daipd@asiainfo.com
# 陈效彩	35812	chenxc@asiainfo.com
# 姚蕾	13285	yaolei2@asiainfo.com
# 王春燕	20298	wangcy7@asiainfo.com
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.delete_all
User.create([{
    name: '季文君',
    email: 'wji@asiainfo.com',
    employee_id: '45721',
    password: 'test123',
    password_confirmation: 'test123',
    admin: true },
    {
        name: '林蓉',
        email: 'linrong@asiainfo.com',
        employee_id: '11306',
        password: 'test123',
        password_confirmation: 'test123',
        manager: true
    }, {
        name: '鲜伟',
        email: 'xianwei@asiainfo.com',
        employee_id: '13730',
        password: 'test123',
        password_confirmation: 'test123',
        manager: true
    }, {
        name: '赵慧敏',
        email: 'zhaohm@asiainfo.com',
        employee_id: '5421',
        password: 'test123',
        password_confirmation: 'test123'
    },
    {
        name: '李阳',
        email: 'liyang@asiainfo.com',
        employee_id: '23389',
        password: 'test123',
        password_confirmation: 'test123'
    },{
        name: '甘雪霞',
        email: 'ganxx@asiainfo.com',
        employee_id: '13188',
        password: 'test123',
        password_confirmation: 'test123'
    },{
        name: '李阳',
        email: 'liyang@asiainfo.com',
        employee_id: '23389',
        password: 'test123',
        password_confirmation: 'test123'
    },{
        name: '朱国栋',
        email: 'zhugd@asiainfo.com',
        employee_id: '13482',
        password: 'test123',
        password_confirmation: 'test123'
    }, {
        name: '李晨升',
        email: 'lics5@asiainfo.com',
        employee_id: '21213',
        password: 'test123',
        password_confirmation: 'test123'
    }, {
        name: '戴鹏达',
        email: 'daipd@asiainfo.com',
        employee_id: '27402',
        password: 'test123',
        password_confirmation: 'test123'
    }, {
        name: '陈效彩',
        email: 'chenxc@asiainfo.com',
        employee_id: '35812',
        password: 'test123',
        password_confirmation: 'test123'
    }, {
        name: '姚蕾',
        email: 'yaolei2@asiainfo.com',
        employee_id: '13285',
        password: 'test123',
        password_confirmation: 'test123'
    }, {
        name: '王春燕',
        email: 'wangcy7@asiainfo.com',
        employee_id: '20298',
        password: 'test123',
        password_confirmation: 'test123'
    }, {
        name: '张宗琦',
        email: 'zhangzq10@asiainfo.com',
        employee_id: '19281',
        password: 'test123',
        password_confirmation: 'test123'
    }, {
        name: '徐岗',
        email: 'xugang5@asiainfo.com',
        employee_id: '21434',
        password: 'test123',
        password_confirmation: 'test123'
    }]
)